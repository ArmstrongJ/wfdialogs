!    Windows Fortran Dialogs (WFDialogs)
!    Copyright 2014 Approximatrix, LLC <support@approximatrix.com>
!
!    WFDialogs is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as 
!    published by the Free Software Foundation, either version 3 of 
!    the License, or (at your option) any later version.
!
!    WFDialogs is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public 
!    License along with WFDialogs.  If not, see 
!    <http://www.gnu.org/licenses/>.

module wfdialogs
    use iso_c_binding
    implicit none
    
    integer(kind=c_int), parameter::MESSAGE_INFO = 1
    integer(kind=c_int), parameter::MESSAGE_WARN = 2
    integer(kind=c_int), parameter::MESSAGE_ERR  = 3
    
    interface 
        function RequestFileOpenDialog_C(title, filename, filelength, maskdesc, mask) bind(c, name="RequestFileOpenDialog")
        use iso_c_binding
            logical(kind=c_bool)::RequestFileOpenDialog_C
            character(kind=C_CHAR)::title(*)
            character(kind=C_CHAR)::filename(*)
            integer(kind=c_int), value::filelength
            character(kind=C_CHAR)::maskdesc(*)
            character(kind=C_CHAR)::mask(*)
        end function RequestFileOpenDialog_C
    end interface
    
    interface
        function RequestFileSaveDialog_C(title, filename, filelength, maskdesc, mask) bind(c, name="RequestFileSaveDialog")
        use iso_c_binding
            logical(kind=c_bool)::RequestFileSaveDialog_C
            character(kind=C_CHAR)::title(*)
            character(kind=C_CHAR)::filename(*)
            integer(kind=c_int), value::filelength
            character(kind=C_CHAR)::maskdesc(*)
            character(kind=C_CHAR)::mask(*)
        end function RequestFileSaveDialog_C
    end interface
    
    interface
        subroutine DisplayMessage_C(message_type, message) bind(c, name="DisplayMessage")
        use iso_c_binding
            integer(kind=c_int)::message_type
            character(kind=C_CHAR)::message(*)
        end subroutine DisplayMessage_C
    end interface
    
    interface
        function DisplayYesNo_C(message_type, message) bind(c, name="DisplayYesNo")
        use iso_c_binding
            logical(kind=c_bool)::DisplayYesNo_C
            integer(kind=c_int)::message_type
            character(kind=C_CHAR)::message(*)
        end function DisplayYesNo_C
    end interface
    
    interface
        function RequestText_C(text, text_length, title, label) bind(c, name="RequestText")
        use iso_c_binding
            logical(kind=c_bool)::RequestText_C
            character(kind=C_CHAR)::text(*)
            integer(kind=c_int), value::text_length
            character(kind=C_CHAR)::title(*)
            character(kind=C_CHAR)::label(*)
        end function RequestText_C
    end interface
    
    interface
        function RequestSelection_C(list, title, label) bind(c,  name="RequestSelection")
        use iso_c_binding
            integer(kind=c_int)::RequestSelection_C
            character(kind=C_CHAR)::list(*)
            character(kind=C_CHAR)::title(*)
            character(kind=C_CHAR)::label(*)
        end function RequestSelection_C
    end interface
    
    contains
    
    subroutine DisplayMessage(message_type, message)
    use iso_c_binding, only: C_NULL_CHAR
    implicit none
    
        integer, intent(in)::message_type
        character(*), intent(in)::message
    
        call DisplayMessage_C(message_type, trim(message)//C_NULL_CHAR)
    
    end subroutine DisplayMessage
    
    function DisplayYesNo(message_type, message)
    use iso_c_binding, only: C_NULL_CHAR
    implicit none
    
        logical::DisplayYesNo
        integer, intent(in)::message_type
        character(*), intent(in)::message
    
        DisplayYesNo = DisplayYesNo_C(message_type, trim(message)//C_NULL_CHAR)
    
    end function DisplayYesNo
    
    function RequestOpenFile(filename, maskdesc, mask, title)
    use iso_c_binding
    implicit none
        
        logical::RequestOpenFile
        character(*), intent(inout)::filename
        character(*), intent(in), optional::maskdesc
        character(*), intent(in), optional::mask
        character(*), intent(in), optional::title
        
        character(64)::internal_maskdesc
        character(64)::internal_mask
        character(64)::internal_title
        
        integer::last_index
        character::last_char
        
        RequestOpenFile = .FALSE.

        last_index = min(len_trim(filename)+1,len(filename))
        last_char = filename(last_index:last_index)
        
        filename(last_index:last_index) = C_NULL_CHAR

        internal_mask = repeat(' ', 64)
        if(present(mask)) then
            internal_mask = mask
        end if
        
        internal_maskdesc = repeat(' ', 64)
        if(present(maskdesc)) then
            internal_maskdesc = maskdesc
        end if
        
        internal_title = repeat(' ', 64)
        if(present(title)) then
            internal_title = title
        else
            internal_title = "Open File"
        end if
        
        if(RequestFileOpenDialog_C(trim(internal_title)//C_NULL_CHAR, &
                                   filename, len(filename), &
                                   trim(internal_maskdesc)//C_NULL_CHAR, &
                                   trim(internal_mask)//C_NULL_CHAR)) then
            RequestOpenFile = .TRUE.
        else
            filename(last_index:last_index) = last_char
        end if
    
    end function RequestOpenFile
    
    function RequestSaveFile(filename, maskdesc, mask, title)
    use iso_c_binding
    implicit none
        
        logical::RequestSaveFile
        character(*), intent(inout)::filename
        character(*), intent(in), optional::maskdesc
        character(*), intent(in), optional::mask
        character(*), intent(in), optional::title
        
        character(64)::internal_maskdesc
        character(64)::internal_mask
        character(64)::internal_title
        
        integer::last_index
        character::last_char
        
        RequestSaveFile = .FALSE.
        
        last_index = min(len_trim(filename)+1,len(filename))
        last_char = filename(last_index:last_index)
        
        filename(last_index:last_index) = C_NULL_CHAR
        
        internal_mask = repeat(' ', 64)
        if(present(mask)) then
            internal_mask = mask
        end if
        
        internal_maskdesc = repeat(' ', 64)
        if(present(maskdesc)) then
            internal_maskdesc = maskdesc
        end if
        
        internal_title = repeat(' ', 64)
        if(present(title)) then
            internal_title = title
        else
            internal_title = "Save File"
        end if
        
        if(RequestFileSaveDialog_C(trim(internal_title)//C_NULL_CHAR, &
                                   filename, len(filename), &
                                   trim(internal_maskdesc)//C_NULL_CHAR, &
                                   trim(internal_mask)//C_NULL_CHAR)) then
            RequestSaveFile = .TRUE.
        else
            filename(last_index:last_index) = last_char
        end if
        
    
    end function RequestSaveFile
    
    function RequestText(text, title, label)
    use iso_c_binding
    implicit none
    
        logical::RequestText
        character(*), intent(inout)::text
        character(*), intent(in), optional::title
        character(*), intent(in), optional::label
        
        character(128)::internal_label
        character(128)::internal_title
        
        integer::last_index
        character::last_char
        
        RequestText = .FALSE.
        
        internal_label = repeat(' ', 128)
        if(present(label)) then
            internal_label = label
        endif
        
        internal_title = repeat(' ', 128)
        if(present(title)) then
            internal_title = title
        else
            internal_title = "Input Requested"
        end if
        
        last_index = min(len_trim(text)+1,len(text))
        last_char = text(last_index:last_index)
        
        text(last_index:last_index) = C_NULL_CHAR
    
        if(RequestText_C(text, len(text), &
                         trim(internal_title)//C_NULL_CHAR, &
                         trim(internal_label)//C_NULL_CHAR)) then
            RequestText = .TRUE.
        else
            text(last_index:last_index) = last_char
        end if
    
    end function RequestText
    
    function RequestSelection(options, selection, title, label)
    use iso_c_binding
    implicit none
    
        logical::RequestSelection
        character(*), dimension(:), intent(in)::options
        integer, intent(out)::selection
        character(*), intent(in), optional::title
        character(*), intent(in), optional::label
        
        character(128)::internal_label
        character(128)::internal_title
        character(:), allocatable::list
        integer::lstart, lend
        integer::list_length, list_pos
        integer::one_length, i
        integer::res
        
        internal_label = repeat(' ', 128)
        if(present(label)) then
            internal_label = label
        endif
        
        internal_title = repeat(' ', 128)
        if(present(title)) then
            internal_title = title
        else
            internal_title = "Input Requested"
        end if
        
        ! Create a list of all options
        lstart = lbound(options, 1)
        lend = ubound(options, 1)
        list_length = (lend - lstart + 1)*(len(options(lstart)) + 1) + 1
        allocate(character(len=list_length) :: list)
        
        list_pos = 1
        do i = lstart, lend
            one_length = len_trim(options(i))
            list(list_pos:(list_pos+one_length+1)) = trim(options(i))//C_NULL_CHAR
            list_pos = list_pos + one_length + 1
        end do
        list(list_pos:list_pos) = C_NULL_CHAR
        
        RequestSelection = .FALSE.
        selection = -1
        res = RequestSelection_C(list, trim(internal_title)//C_NULL_CHAR, &
                                 trim(internal_title)//C_NULL_CHAR)
        if(res .GE. 0) then
            selection = res + lstart
            RequestSelection = .TRUE.
        end if
    
    end function RequestSelection
    
end module wfdialogs

!    Windows Fortran Dialogs (WFDialogs)
!    Copyright 2014 Approximatrix, LLC <support@approximatrix.com>
!
!    WFDialogs is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as 
!    published by the Free Software Foundation, either version 3 of 
!    the License, or (at your option) any later version.
!
!    WFDialogs is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public 
!    License along with WFDialogs.  If not, see 
!    <http://www.gnu.org/licenses/>.

program askforfile
use wfdialogs
implicit none

character(256)::filename
character(256)::text
character(64), dimension(5)::options
integer::selection

    filename = repeat(' ', 256)
    filename = "C:\"
    
    if(RequestOpenFile(filename, "Text Files", "*.txt")) then
        call DisplayMessage(MESSAGE_INFO, "The file '"//trim(filename)//"' was selected")
    else
        call DisplayMessage(MESSAGE_WARN, "No file was selected!")
    end if
    
    if(DisplayYesNo(MESSAGE_INFO, "Are you happy with this result?")) then
        call DisplayMessage(MESSAGE_INFO, "Good!")
    else
        call DisplayMessage(MESSAGE_ERR, "I'm sorry!")
    end if
    
    text = "Your name, please"
    if(RequestText(text, title="Name Entry", label="Please enter your name below:")) then
        Print *, "Thank you, "//trim(text)
    else
        Print *, "Oh well"
    endif
    
    options(1) = "one"
    options(2) = "two"
    options(3) = "three"
    options(4) = "four"
    options(5) = "five"
    
    if(RequestSelection(options, selection, title="A Choice", label="Select a number:")) then
        Print *, "You've chosen "//options(selection)//"!"
    else
        Print *, "No selection"
    end if
    
end program askforfile

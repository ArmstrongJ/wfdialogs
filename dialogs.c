/* Windows Fortran Dialogs (WFDialogs)
 * Copyright 2014 Approximatrix, LLC <support@approximatrix.com>
 * 
 * WFDialogs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 * 
 * WFDialogs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with WFDialogs.  If not, see 
 * <http://www.gnu.org/licenses/>.
 */

#include <windows.h>
#include <stdbool.h>

static bool RequestFileDialog(bool isOpen, const char *title, char *filename, int filelength, const char *maskdesc, const char *mask)
{
OPENFILENAME ofn;
char *allmasks;
char *pos;
bool ret;

char *tempfile;
char *temppath;

    if(filename == NULL) return NULL;

    allmasks = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 512*sizeof(char));
    if(allmasks == NULL) return false;
    
    if(mask != NULL && strlen(mask) > 0) {
        if(maskdesc != NULL && strlen(maskdesc) > 0)
            strncat(allmasks, maskdesc, 128);
        else {
            strncat(allmasks, mask, 32);
            strncat(allmasks, "files", 32);
        }
        pos = allmasks+strlen(allmasks) + 1;
        strncat(pos, mask, 128);
        pos = pos + strlen(mask) + 1;
    } else
        pos = allmasks;
        
    memcpy(pos, "All Files\0*.*", 13);
    
    tempfile = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, filelength*sizeof(char));
    if(tempfile == NULL) {
        HeapFree(GetProcessHeap(), 0, allmasks);
        return false;
    }
    
    temppath = NULL;
    if(strlen(filename) > 0) {
        if(filename[strlen(filename)-1] == '/' || filename[strlen(filename)-1] == '\\') {
            temppath = filename;
        } else {
            strcpy(tempfile, filename);
        }
    }
    
    ZeroMemory(&ofn,sizeof(ofn));
    
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = NULL;
    ofn.hInstance = NULL;
    ofn.lpstrFile = tempfile;
    ofn.nMaxFile = filelength-1;
    ofn.Flags = OFN_EXPLORER;
    ofn.lpstrFilter = allmasks;
    ofn.lpstrTitle = title;
    ofn.lpstrInitialDir = temppath;

    if(isOpen) 
        ret = (bool)GetOpenFileName(&ofn);
    else
        ret = (bool)GetSaveFileName(&ofn);

    HeapFree(GetProcessHeap(), 0, allmasks);

    if(ret) {
        strcpy(filename, tempfile);
    }

    HeapFree(GetProcessHeap(), 0, tempfile);

    /* Fortran is not a fan of null characters, generally */
    filename[strlen(filename)] = ' ';

    return ret;
}

bool RequestFileOpenDialog(const char *title, char *filename, int filelength, const char *maskdesc, const char *mask)
{
    return RequestFileDialog(true, title, filename, filelength, maskdesc, mask);
}

bool RequestFileSaveDialog(const char *title, char *filename, int filelength, const char *maskdesc, const char *mask)
{
    return RequestFileDialog(false, title, filename, filelength, maskdesc, mask);
}

static int DisplayMessageBox(int type, const char *message, DWORD flags)
{
const char *title;

    if(message == NULL) return;
    
    switch(type) {
        case 1:
            title = "Information";
            flags |= MB_ICONINFORMATION;
            break;
        case 2:
            title = "Warning";
            flags |= MB_ICONWARNING;
            break;
        case 3:
            title = "Error";
            flags |= MB_ICONERROR;
            break;
        default:
            title = "Message";
    }
    
    return MessageBox(0, message, title, flags);
}

void DisplayMessage(int type, const char *message)
{
    DisplayMessageBox(type, message, MB_OK);
}

bool DisplayYesNo(int type, const char *message)
{
    return (DisplayMessageBox(type, message, MB_YESNO) == IDYES);
}

struct passingToDialog {
    const char *title;
    char *text;
    int text_length;
    const char *label;
};

static BOOL CALLBACK TextEntryDialog(HWND hwndDlg, UINT uMsg,
                                     WPARAM wParam, LPARAM lParam)
{
static struct passingToDialog *passed;

    switch(uMsg)
    {
        case WM_INITDIALOG:
            passed = (struct passingToDialog *)lParam;
            
            SetDlgItemText(hwndDlg, 1001, passed->label);
            SetDlgItemText(hwndDlg, 1002, passed->text);
            
            SetWindowText(hwndDlg, passed->title);
            
            break;
            
        case WM_COMMAND:
            if (LOWORD(wParam) == IDOK) {
                GetDlgItemText(hwndDlg, 1002, passed->text, passed->text_length-1);
                
                EndDialog(hwndDlg, TRUE);
                return TRUE;
            } else if (LOWORD(wParam) == IDCANCEL) {
                EndDialog(hwndDlg, FALSE);
                return TRUE;
            }
            break;
        case WM_CLOSE:  /* the dialog is closed */
            EndDialog(hwndDlg, FALSE);
            return TRUE;

        default:
            return FALSE;
    }
    return FALSE;
}

static BOOL CALLBACK ListSelectDialog(HWND hwndDlg, UINT uMsg,
                                      WPARAM wParam, LPARAM lParam)
{
HWND hChild;
static struct passingToDialog *passed;
char *walk;

    switch(uMsg)
    {
        case WM_INITDIALOG:
            passed = (struct passingToDialog *)lParam;
            
            SetDlgItemText(hwndDlg, 1001, passed->label);
            hChild = GetDlgItem(hwndDlg, 1002);
            walk = passed->text;
            while(strlen(walk) > 0) {
                SendMessage(hChild, CB_ADDSTRING, (WPARAM)0, (LPARAM)walk);
                walk += (strlen(walk)+1);
            }
            SendMessage(hChild, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
            
            SetWindowText(hwndDlg, passed->title);
            
            break;
            
        case WM_COMMAND:
            if (LOWORD(wParam) == IDOK) {
                hChild = GetDlgItem(hwndDlg, 1002);
                EndDialog(hwndDlg, SendMessage(hChild, CB_GETCURSEL, (WPARAM)0, (LPARAM)0));
                return TRUE;
            } else if (LOWORD(wParam) == IDCANCEL) {
                EndDialog(hwndDlg, -1);
                return TRUE;
            }
            break;
        case WM_CLOSE:  /* the dialog is closed */
            EndDialog(hwndDlg, -1);
            return TRUE;

        default:
            return FALSE;
    }
    return FALSE;
}

bool RequestText(char *text, int text_length, const char *title, const char *label)
{
struct passingToDialog passing;
int res;
int i;

    passing.title = title;
    passing.label = label;
    passing.text = text;
    passing.text_length = text_length;
    
    res = DialogBoxParam(NULL, "TextEntry", NULL, TextEntryDialog, (LPARAM)&passing);
    
    if(res) {
        for(i = strlen(text); i < text_length; i++)
            text[i] = ' ';
    }
    
    return (bool)res;
}

int RequestSelection(char *items, const char *title, const char *label)
{
struct passingToDialog passing;

    passing.title = title;
    passing.label = label;
    passing.text = items;
    
    return DialogBoxParam(NULL, "ListSelect", NULL, ListSelectDialog, (LPARAM)&passing);
}
Windows Fortran Dialogs
=======================

This small library is designed to provide a handful of common dialogs
on Microsoft Windows and compatible operating systems from within
Fortran.

Usage
-----

To use the library in any given Fortran program, simply add:

```
use wfdialogs
```

to your program or routine.

You'll need to link against comdlg32 when compiling.  Add the linker flag:

```
-lwfdialogs -lcomdlg32
```

when building anything that uses these routines.

The following functions are provided:

### DisplayMessage

Displays a message in a window with an Ok button

**Syntax:** 
```
call DisplayMessage(message_type, message)
```

**Arguments:**

Argument    |   Type     |   Description
------------|------------|---------------
message_type|integer     |The type of message, one of: *MESSAGE_INFO*, *MESSAGE_WARN*, or *MESSAGE_ERR*
message     |character(*)|The message to display


### DisplayYesNo

Displays a message in a window with a Yes and No button

**Syntax:** 
```
res = DisplayYesNo(message_type, message)
```

**Arguments:**

Argument    |   Type     |   Description
------------|------------|---------------
message_type|integer     |The type of message, one of: *MESSAGE_INFO*, *MESSAGE_WARN*, or *MESSAGE_ERR*
message     |character(*)|The message to display

**Returns:**
*.TRUE.* if "Yes" was clicked, *.FALSE.* otherwise

### RequestOpenFile, RequestSaveFile

Opens a standard file selection dialog

**Syntax:** 
```
res = RequestOpenFile(filename, maskdesc, mask, title)
res = RequestSaveFile(filename, maskdesc, mask, title)
```

**Arguments:**

Argument  |   Type                    |   Description
----------|---------------------------|---------------
filename  |character(*), intent(inout)| The initial filename or path with which to populate the file dialog.  Contains the selected file if successful
maskdesc  |character(*), optional     | A description of the provided file mask
mask      |character(*), optional     | An optional file maskj.  Does not need to be accompanied by *maskdesc*.
title     |character(*), optional     | The title to display on the file selection dialog

**Returns:**
*.TRUE.* if a file was selected, *.FALSE.* otherwise

### RequestSelection

Allows a user to select an item from a list in a popup dialog box

**Syntax:** 
```
function RequestSelection(options, selection, title, label)
```

**Arguments:**

Argument  |   Type                    |   Description
----------|---------------------------|---------------
options   |character(*), dimension(:) | An array of strings with which to populate the list
selection |integer, intent(out)       | If successful, returns the *1-based* index of the selected item
title     |character(*), optional     | The title to display on the popup dialog box
label     |character(*), optional     | The label to display above the list in the popup dialog box

**Returns:**
*.TRUE.* if the Ok button was clicked, *.FALSE.* otherwise. *selection* is valid only if *.TRUE.* is returned.

### RequestText

Allows a user to enter text in a popup dialog box

**Syntax:** 
```
function RequestText(text, title, label)
```

**Arguments:**

Argument  |   Type                    |   Description
----------|---------------------------|---------------
text      |character(*), intent(inout)| Initial text to display.  If successful, contains the user-entered text.
title     |character(*), optional     | The title to display on the popup dialog box
label     |character(*), optional     | The label to display above the text entry in the popup dialog box

**Returns:**
*.TRUE.* if the Ok button was clicked, *.FALSE.* otherwise

Licensing
---------

Windows Fortran Dialogs (WFDialogs)
Copyright 2014 Approximatrix, LLC <support@approximatrix.com>

WFDialogs is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

WFDialogs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with WFDialogs.  If not, see 
<http://www.gnu.org/licenses/>.